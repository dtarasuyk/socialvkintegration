<?php

Namespace SocialVkIntegration;
/**
 * Created by PhpStorm.
 * User: ychenik
 * Date: 24.1.16
 * Time: 13.19
 */
use GuzzleHttp\Client  as HttpClient;

class SocialVkIntegration
{
    private $client_id;
    private $user_id;
    private $access_key;
    private $display;
    private $http;
    private $code;
    private $client_secret;
    private $score=[];
    private $client;
    public function __construct($cl_id,$dis,$url,$sc,$key)
    {
        $this->client_id=$cl_id;
        $this->display=$dis;
        $this->http=$url;
        $this->score=$sc;
        $this->client_secret=$key;
	$this->client = new HttpClient(['base_uri' => 'https://oauth.vk.com/access_token']);
    }

    public function setClientId($cl_id){
        $this->client_id=$cl_id;
    }
    public function setClientSecred($key){
        $this->client_secret=$key;
    }
    public function setDisplay($dis){
        $this->display=$dis;
    }
    public function setHTTP($url){
        $this->http=$url;
    }
    public function setScore($sc){
        $this->score=$sc;
    }
    public function setCode($code){
        $this->code=$code;
    }
    public function getHTTP(){
        return $this->http;
    }
    public function getAccess(){
        $res=$this->client->request('GET',"https://oauth.vk.com/access_token?".http_build_query([
                "client_id"=>$this->client_id,
                "client_secret"=>$this->client_secret,
                "redirect_uri"=>$this->http,
                "code"=>$this->code])
        );
	$arr=json_decode($res->getBody()->getContents());
	$this->checkingForErrors($q->error->error_code);
        $this->access_key=$arr->access_token;
        $this->user_id=$arr->user_id;
    }
    public function callApi($methodName, $methodParams){
	    $res=$this->client->request('GET',
            sprintf("https://api.vk.com/method/%s?user_id=%d&fields=%s&v=5.44&access_token=%s",
                $methodName,
                $this->user_id,
                implode(',',$methodParams),
                $this->access_key
            ));
	$q=json_decode($res->getBody()->getContents());
	$this->checkingForErrors($q->error->error_code);
	return $q;
    }
    public function getCode(){
        return sprintf(
            "https://oauth.vk.com/authorize?client_id=%d&display=%s&redirect_uri=%s&score=%s&response_type=code",
            $this->client_id,
            $this->display,
            $this->http,
            implode(',',$this->score)
        );
    }
    private function checkingForErrors($code){
        switch($code){
            case 1:
                throw new Exception\UnknownErrorException();
                break;
            case 2:
                throw new Exception\AppendixOffException();
                break;
            case 3:
                throw new Exception\UnknownMethodException();
                break;
            case 4:
                throw new Exception\InvalidSignatureException();
                break;
            case 5:
                throw new Exception\AuthorizationFailedException();
                break;
            case 6:
                throw new Exception\ManyRequestsInSecondException();
                break;
            case 7:
                throw new Exception\NoLawException();
                break;
            case 8:
                throw new Exception\InvalidQueryException();
                break;
            case 9:
                throw new Exception\ManySimilarActionException();
                break;
            case 10:
                throw new Exception\InternalServerErrorException();
                break;
            case 14:
                throw new Exception\RequiredCodeFromImageException();
                break;
            case 15:
                throw new Exception\AccessDeniedException();
                break;
            case 17:
                throw new Exception\RequiresUserValidationException();
                break;
            case 20:
                throw new Exception\ActionDeniedForOnlineAppException();
                break;
            case 21:
                throw new Exception\ActionAllowedForOnlineAppException();
                break;
            case 23:
                throw new Exception\MethodOffException();
                break;
            case 24:
                throw new Exception\UserMustConfirmException();
                break;
            case 100:
                throw new Exception\RequiresUserValidationException();
                break;
            case 101:
                throw new Exception\InvalidApiIdApplicationsException();
                break;
            case 113:
                throw new Exception\InvalidUserIDException();
                break;
            case 150:
                throw new Exception\InvalidTimestampException();
                break;
            case 200:
                throw new Exception\AccessDeniedToAlbumException();
                break;
            case 201:
                throw new Exception\AccessDeniedAudio();
                break;
            case 203:
                throw new Exception\GroupAccessDenied();
                break;
            case 300:
                throw new Exception\AlbumIsFull();
                break;
            case 500:
                throw new Exception\ActionProhibited();
                break;
            case 600:
                throw new Exception\NoPermissionToWorkWithAdvertisingOffice();
                break;
            case 603:
                throw new Exception\BugWithAdvertisingAccount();
                break;
        }
    }
}
